angular.module('config', [])
  .constant('PAGES', [
    { name: 'users', label: 'Users' },
  ])

const app = angular.module('myApp', [
  'config', 'users', 'tasks', 'settings', 'ui.router'
])

// Global scope
app.config(function (PAGES) {
  // console.log(PAGES)
})
app.run(function ($rootScope, PAGES) { })



angular.module('myApp').config(function (UsersServiceProvider) {
  UsersServiceProvider.setUsersUrl('http://localhost:3000/users')
})

angular.module('myApp').run(function (UsersService) {
  // UsersService.fetchUsers()
})

app.config(function ($httpProvider) {
  $httpProvider.interceptors.push(function ($q, $rootScope, $timeout) {
    return {
      'request': function (config) {
        config.headers['X-Placki'] = 'true'
        return config;
      },

      'responseError': function (response) {
        $rootScope.message = 'Server error'
        $timeout(() => {
          $rootScope.message = ''
        }, 2000)

        return $q.reject(response);
      }
    };
  });
})

app.filter('yesno', function (/* service */) {
  return function (val) {
    return val ? 'Yes' : 'No'
  }
})


app.controller('AppCtrl', ($scope, PAGES) => {
  $scope.title = 'MyApp'
  $scope.user = { name: 'Guest' }

  $scope.isLoggedIn = false;

  $scope.login = () => {
    $scope.user.name = 'Admin'
    $scope.isLoggedIn = true
  }

  $scope.logout = () => {
    $scope.user.name = 'Guest'
    $scope.isLoggedIn = false
  }

  $scope.pages = PAGES

  $scope.currentPage = $scope.pages[1]

  $scope.goToPage = pageName => {
    $scope.currentPage = $scope.pages.find(p => p.name === pageName)
  }
})

/* UI STATE ROUTER */
// https://ui-router.github.io/ng1/
// npm install --save @uirouter/angularjs

app.config(function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/users/')
  // $urlRouterProvider.when('','/users')
  $stateProvider
    .state({
      name: 'users',
      url: '/users/',
      resolve: {
        users(UsersService) {
          return UsersService.fetchUsers()
        }
      },
      component: 'usersPage',
    })
    .state({
      name: 'users.details',
      // url: '/users/:userId',
      url: 'show/:userId',
      // data: {
      //   mode: 'details',
      // },
      resolve: {
        mode() { return 'details' },
        // users(UsersService) {
        //   return UsersService.fetchUsers()
        // },
        user(UsersService, $transition$) {
          const id = $transition$.params().userId
          return UsersService.fetchUserById(id)
        },
        onEdit($state) {
          return () => { $state.go('users.edit') }
        }
      },
      component: 'userDetails'
      // component: 'usersPage'
    })
    .state({
      name: 'users.edit',
      // url: '/users/:userId',
      url: 'edit/:userId',
      // data: {
      //   mode: 'details',
      // },
      resolve: {
        mode() { return 'edit' },
        // users(UsersService) {
        //   return UsersService.fetchUsers()
        // },
        user(UsersService, $transition$) {
          const id = $transition$.params().userId
          return UsersService.fetchUserById(id)
        },
        onCancel($state) {
          return () => { $state.go('^') }
        }
      },
      component: 'userEdit'
      // component: 'usersPage'
    })
    // .state({
    //   name: 'users.edit',
    //   url: 'edit/:userId',
    //   template: '<h1>Placki {{ $ctrl.placki }} </h1>',
    //   controller: function ($stateParams) {
    //     this.placki = $stateParams.userId
    //   },
    //   controllerAs: '$ctrl',
    // })
    .state({
      name: 'tasks',
      url: '/tasks',
      component: 'tasksPage'
    })
    .state({
      name: 'settings',
      url: '/settings',
      templateUrl: '/views/settings-view.tpl.html'
      // template: '<h1>users</h1>'
      // templateUrl:'/views/users-view.tpl.html'
      // template:'<users-page><users-page>'
    })
  $urlRouterProvider.otherwise('users')
})
app.run(function ($state) {
  // $state.go('users')
})