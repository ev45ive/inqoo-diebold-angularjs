
const users = angular.module('users', ['users.service'])

angular.module('users')
  .component('usersPage', {
    bindings: {
      users: '<',
      selected: '<',
      mode: '<'
    },
    template:/* html */`<div>
      <div class="row">
        <div class="col">
        <button class="btn btn-info float-end" ng-click="$ctrl.create()">Create new</button>
        <h4 ng-click="{}">Users</h4>

          <users-list users="$ctrl.users" 
            selected="$ctrl.selected"
            on-delete="$ctrl.delete($event)" 
            on-select="$ctrl.select($event)"></<users-list> 
            
        </div>
        <div class="col"> 
          <!-- <user-details 
            ng-if="$ctrl.selected && $ctrl.mode == 'details'"
            user="$ctrl.selected" on-edit="$ctrl.edit($event)" on-cancel="$ctrl.cancel()">
            </user-details> -->
            <ui-view></ui-view>

           <!--<user-edit 
            ng-if="$ctrl.selected && $ctrl.mode == 'edit'"
            user="$ctrl.selected" 
            on-save="$ctrl.save($event)" 
            on-cancel="$ctrl.cancel()"></user-edit>

          <user-edit 
            ng-if="$ctrl.mode == 'create'"
            user="{}" 
            on-save="$ctrl.saveNew($event)" 
            on-cancel="$ctrl.cancel()"></user-edit>-->
        </div>
      </div>
    </div>`,
    controller: class UsersPageCtrl {
      /** @type User */
      // selected = null

      /** @type User[] */
      // users = []

      constructor(UsersService, $q, $state) {
        this.$q = $q
        this.$state = $state
        this.service = UsersService
        // this.mode = 'details'
      }
      $onInit() {
        // debugger
        // this;
        // this.refresh() 
      }

      refresh() {
        this.service.fetchUsers().then(users => {
          this.users = users
        })
      }
      select(id) {
        // this.mode = 'details';
        // this.$state.go('users-details', { userId: id })
        this.$state.go('users.details', { userId: id })
      }
      create() {
        this.mode = 'create'
      }
      edit(id) {
        return this.service.fetchUserById(id).then(user => {
          this.mode = 'edit';
          this.selected = user
        })
      }
      cancel() {
        this.mode = 'details'; this.selected = null
      }

      delete(id) {
        this.service.deleteUserById(id).then(() => {
          this.selected = null
          return this.refresh()
        })
      }

      saveNew(draft) {
        this.service.saveNewUser(draft)
          .then(result => {
            return this.$q.all([this.refresh(), this.select(result.id)])
          })
        // .then(() => this.messages.show('Changes saved!')
      }

      save(draft) {
        this.service.saveUpdatedUser(draft)
          .then(result => {
            return this.$q.all([this.refresh(), this.select(result.id)])
          })
        // .then(() => this.messages.show('Changes saved!')
      }
    }
  })
  .component('usersList', {
    bindings: { users: '<', onSelect: "&", onDelete: "&", selected: '<' },
    // ng-click="$ctrl.onSelect({$event: user.id})" 
    template:/* html */`<div>
    <div class="list-group-item" 
        ui-sref-active="active"
        ui-sref="users.details({userId:user.id})"
        ng-repeat="user in $ctrl.users track by user.id"
        ng-class="{'active':user.id == $ctrl.selected.id}">
        <span class="close float-end" ng-click="
          $event.stopPropagation(); $ctrl.onDelete({$event: user.id})">&times;</span>
        {{$index+1}}. {{user.name}}
        <small class="text-muted">{{user.email}}</small>
      </div>
    </div>`,
    controller: class {
      $ngInit() {

      }
    }
  })

  .component('userEdit', {
    bindings: { user: '<', onSave: '&', onCancel: '&' },
    template:/* html */`<div>
    <form name="$ctrl.userForm" novalidate ng-submit="$ctrl.save()"> <h4>Edit user </h4>
      <div class="form-group mb-4">
        <label for="">Name</label>
        <input type="text" class="form-control" ng-model="$ctrl.draft.name" name="name" required minlength="3">
        <div
          ng-if="$ctrl.userForm['name'].$dirty || $ctrl.userForm['name'].$touched || $ctrl.userForm.$submitted">
          <p class="text-danger" ng-if="$ctrl.userForm['name'].$error.minlength">Name is too short!</p>
          <p class="text-danger" ng-if="$ctrl.userForm['name'].$error.required">Name is required!</p>
        </div>
      </div>
      <div class="form-group mb-4">
        <label for="">Email</label>
        <input type="email" class="form-control" ng-model="$ctrl.draft.email" name="email" required>

        <div
          ng-if="$ctrl.userForm['email'].$dirty || $ctrl.userForm['email'].$touched || $ctrl.userForm.$submitted">
          <p class="text-danger" ng-if="$ctrl.userForm['email'].$error.required">Email is required !</p>
          <p class="text-danger" ng-if="$ctrl.userForm['email'].$error.email">Email invalid !</p>
        </div>
      </div>
      <button class="btn btn-danger" type="button" ng-click="$ctrl.onCancel()">Cancel</button>
      <button class="btn btn-success" type="submit">Save</button>
       </form>
    </div>`,
    controller: class UserEditFormCtrl {
      $onInit() { }
      $onChanges() {
        this.draft = { ...this.user }
      }
      save() {
        if (this.userForm.$invalid) { return }

        this.onSave({
          $event: {
            ...this.user,
            ...this.draft
          }
        })
      }
    }
  })

  .component('userDetails', {
    bindings: { user: '<', onEdit: '&', onCancel: '&' },
    template:/* html */`<div><h4>User details</h4>
      <dl>
        <dt>Name</dt>
        <dd>{{$ctrl.user.name}}</dd>
        <dt>E-mail</dt>
        <dd>{{$ctrl.user.email}}</dd>
      </dl>
      <button ui-sref="users.edit({userId:$ctrl.user.id})">Edit</button>
      <button ng-click="$ctrl.onCancel()">Cancel</button>
      </div>`,
    // <button ng-click="$ctrl.onEdit({$event:$ctrl.user.id})">Edit</button>
    controller() { }
  })

/**
 * @typedef User
 * @property {string} username
 */
angular.module('users')
  .controller('UsersPageCtrl', [
    '$scope', '$timeout', '$q', 'UsersService',
    ($scope, $timeout, $q, UsersService) => {
      /** @type MyScope */
      // const this = $scope.usersPage = {}

      this.selected = null
      this.draft

      this.refresh = () => {
        return UsersService.fetchUsers()
          .then((data) => this.users = data)
      }
      this.refresh()

      this.create = () => {
        this.selected = {}
        this.draft = {}
        this.mode = 'edit'
      }

      this.select = (id) => {
        return UsersService.fetchUserById(id).then(user => {
          this.selected = user
          this.mode = 'details'
        })
      }

      this.edit = () => {
        this.mode = 'edit'
        this.draft = { ...this.selected }
      }
      this.select('1').then(() => this.edit())

      this.save = (draft) => {
        if (this.userForm.$invalid) {
          this.showMessage('Form has errors')
          return
        }
        const result =
          draft.id ? UsersService.saveUpdatedUser(draft) : UsersService.saveNewUser(draft)

        return result.then(saved => {
          return $q.all([this.select(saved.id), this.refresh()])
        })
          .then(() => this.showMessage('Changes Saved'))
      }

      this.showMessage = msg => {
        this.message = msg;
        $timeout(() => {
          this.message = ''
        }, 2000)
      }

    }])
  // .controller('UserListCtrl', ($scope) => {
  //   const this = $scope.list = {}
  //   this.filtered = []
  //   this.select = (id) => $scope.$emit('selectUser', id)

  // })
  // .controller('UserDetailsCtrl', ($scope) => {
  //   const this = $scope.details = {}
  //   this.user = null

  //   $scope.$on('userSelected', (event, user) => { this.user = (user) })

  // })
  // .controller('UserEditFormCtrl', ($scope) => {
  //   const this = $scope.editform = {}
  //   this.draft = {}
  // })
