//@ts-check

// https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html
/** @typedef  {import("@uirouter/angularjs").StateProvider} StateProvider */
/** @typedef  {import("@uirouter/angularjs").UrlRouterProvider} UrlRouterProvider */
/** @typedef  {import("@uirouter/core").Transition} Transition */
/** @typedef  {import("angular").IHttpService} IHttpService */



angular.module('myApp', ['ui.router', 'email'])
angular.module('myApp')
    .config(['$stateProvider', '$urlRouterProvider', (
        /** @type {StateProvider} */
        $stateProvider,
        /** @type {UrlRouterProvider} */
        $urlRouterProvider
    ) => {

        $urlRouterProvider.when('', '/')

        $stateProvider.state({
            name: 'home',
            template: 'Hello home',
            url: '/'
        })

        $stateProvider.state('messages', {
            url: '/messages',
            resolve: {
                title: () => 'Inbox',
                messages(/** @type {MessagesService} */ MessagesService) {
                    return MessagesService.getMessages()
                }
            },
            component: 'messagesList'
        })
            .state('create', {
                parent: 'messages',
                url: '/create',
                template: ' Create new message '
            })
            .state('messages.details', {
                // parent: 'messages',
                url: '/{messageId}',
                resolve: {
                    message(
                      /** @type {MessagesService} */ MessagesService,
                      /** @type {Transition} */  $transition$
                    ) {
                        return MessagesService.getMessageById(
                            $transition$.params('to').messageId
                        )
                    }
                },
                component: 'messageDetail'
            })

        // $stateProvider.state('messages.details', {
        //     url: '/',
        //     template: '<p class="alert alert-info>No messages selected</p>'
        // })

    }])
    .run(() => {
        console.log('Angular rootScope ready')
    })