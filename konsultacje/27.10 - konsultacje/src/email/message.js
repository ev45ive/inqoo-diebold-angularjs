
angular.module('email')
    .component('messageDetail', {
        bindings: {
            message: '<'
        },
        template: /* html */ `<div>
            <h3>{{$ctrl.message.subject}}</h3>
            <hr/>
            <p>{{$ctrl.message.body}}</p>
        </div>`
    })