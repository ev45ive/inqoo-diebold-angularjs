
angular.module('email', [])
    .component('messagesList', {
        bindings: {
            'title': '<',
            'messages': '<',
        },
        template: /* html */ `<div>
            <h3>{{$ctrl.title }}</h3>
            <div class="row">
                <div class="col">
                    <div class="list-group">
                        <div class="list-group-item" 
                        ng-repeat="msg in $ctrl.messages"
                        ui-sref="messages.details({messageId: msg.id})">
                        {{msg.subject}}
                        </div>
                    </div>
                </div>
                <div class="col">
                    <ui-view></ui-view>
                </div>
            </div>
        </div>`,
        controller() {
            this.title = ''
            this.messages = []

            this.$onInit = function () {
                console.log(this)
            }
        }
    })