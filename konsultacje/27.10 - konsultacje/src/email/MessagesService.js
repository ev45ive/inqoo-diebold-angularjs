/** 
 * @typedef Message 
 * @property  {string} id
 * @property  {string} subject
 * @property  {string} body
 */

class MessagesService {

    constructor(/** @type {IHttpService} */ $http) {
        this.$http = $http;
    }

    /**
     * @param {string} id 
     * @returns {Promise<Message[]>}
     */
    getMessages() {
        return this.$http.get('/messages').then(res => res.data);
    }

    /**
     * @param {string} id 
     * @returns {Promise<Message>}
     */
    getMessageById(id) {
        return this.$http.get('/messages/' + id).then(res => res.data);
    }
}
angular.module('email').service('MessagesService', MessagesService);
