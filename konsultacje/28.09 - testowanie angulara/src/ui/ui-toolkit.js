
angular.module('ui-toolkit', [])
  .component('uiToolkitDemo', {
    template:/* html */`<div ng-init="zmienna = {msg:'hello'} ">

      <div ui-tooltip="I am a tooltip!" class="m-2 p-4 border mb-3" >
        <h4>Hover me for tooltip</h4>
        <input type="text" ng-model="zmienna.msg">
      </div>

      <button ui-tooltip="I am a tooltip!" on-tooltip-close="zmienna.msg = 'closed' " class="btn btn-info">Hover me for tooltip</button>

      
      </div>`
  })

  .directive('uiTooltip', function () {

    return {
      restrict: 'A',
      link(scope, $element, attrs, ctrl) {
        console.log($element)

        // const tooltipElem = document.createElement('div')
        const tooltipElem = angular.element(/* html */`<div class="bg-dark rounded text-light p-1"> 
          Tooltip Test <span class="close">&times;</span>
         </div>`)

        tooltipElem.on('click', '.close', () => {
          scope.$apply(() => {

            scope.$eval(attrs['onTooltipClose'], { $event: 'placki' })
          })
          // scope.$digest()
        })

        let handle

        $element.on('mouseenter', (event) => {
          $element.after(tooltipElem)
          clearTimeout(handle)
        })


        $element.on('mouseleave', (event) => {
          clearTimeout(handle)

          handle = setTimeout(() => {
            tooltipElem.remove()
          }, 2000);
        })
      }
    }
  })


  // jQuery
  // $('.nav.bs-sidenav a').not('[href=#popovers]').css('color','red')